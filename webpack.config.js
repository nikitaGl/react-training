const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './app/index.html',
    filename: 'index.html',
    inject: 'body'
});

module.exports = {
    entry: './app/index.jsx',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        loaders: [
            { test: /(\.js|\.jsx)$/, loader: 'babel-loader', exclude: /node_modules/ },
            { test: /(\.js|\.jsx)$/, loader: 'babel-loader', include: path.resolve(__dirname, './node_modules/react-icons/fa')},
            { test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader'},
            { test: /\.svg$/, loader: 'file-loader'},
        ]
    },
    plugins: [HtmlWebpackPluginConfig]
};