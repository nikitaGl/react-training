import * as types from '../shared/actionTypes.jsx';

export const updateRates = (fromUnit, toUnit, rate) => ({type: types.UPDATE_RATES, fromUnit, toUnit, rate});

export const addLogMessage = (message) => ({type: types.ADD_LOG_MESSAGE, message});
export const toggleLogger = (isOpen) => ({type: types.TOGGLE_LOGGER, isOpen});
export const revertLastChange = () => ({type: types.REVERT_LAST_CHANGE});