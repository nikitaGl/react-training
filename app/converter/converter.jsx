import {MoneyConverter, TimeConverter, LengthConverter, MassConverter} from  './units-converters';

export class Converter {
    constructor(type, units) {
        switch(type) {
            case 'money':
                this.converter = new MoneyConverter(units);
                break;
            case 'time':
                this.converter = new TimeConverter(units);
                break;
            case 'length':
                this.converter = new LengthConverter(units);
                break;
            case 'mass':
                this.converter = new MassConverter(units);
                break;
            default:
                this.units = null;
                this.converter = null;
                break;
        }
    }

    createConverter(settings) {
        this.converter.changeUnits(settings);
        return this.converter;
    }
}