export const MONEY_UNITS = new Map([
    ['BYN', new Map([
        ['BYN', 1],
        ['RUB', 1],
        ['USD', 2],
        ['EUR', 3],
        ['UAH', 4],
        ['GBP', 5]
    ])],
    ['RUB', new Map([
        ['BYN', 1],
        ['RUB', 1],
        ['USD', 2],
        ['EUR', 3],
        ['UAH', 4],
        ['GBP', 5]
    ])],
    ['USD', new Map([
        ['BYN', 1],
        ['RUB', 1],
        ['USD', 2],
        ['EUR', 3],
        ['UAH', 4],
        ['GBP', 5]
    ])],
    ['EUR', new Map([
        ['BYN', 1],
        ['RUB', 1],
        ['USD', 2],
        ['EUR', 3],
        ['UAH', 4],
        ['GBP', 5]
    ])],
    ['UAH', new Map([
        ['BYN', 1],
        ['RUB', 1],
        ['USD', 2],
        ['EUR', 3],
        ['UAH', 4],
        ['GBP', 5]
    ])],
    ['GBP', new Map([
        ['BYN', 1],
        ['RUB', 1],
        ['USD', 2],
        ['EUR', 3],
        ['UAH', 4],
        ['GBP', 5]
    ])]
]);
