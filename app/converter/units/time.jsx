export const TIME_UNITS = new Map([
    ['second', 1],
    ['minute', 60],
    ['hour', 60*60],
    ['day', 60*60*24]
]);