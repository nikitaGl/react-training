export class LengthConverter {
    constructor(units) {
        this.units = units;
    }

    changeUnits(settings) {
        this.from = settings.from;
        this.to = settings.to;
    }

    convert(value) {
        let result = (value * this.units.get(this.from)) / this.units.get(this.to);
        return `${value} ${this.from} = ${result} ${this.to}`;
    }
}
