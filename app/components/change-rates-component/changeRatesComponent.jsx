import React from 'react';
import uuid from 'uuid';
import './changeRatesComponent.scss'
import FaArrowsV from 'react-icons/lib/fa/arrows-v';
import FaArrowRight from 'react-icons/lib/fa/arrow-right'

export default class ChangeRatesComponent extends React.Component {
  constructor(props) {
    super(props);

    let options = props.units ? Array.from(props.units.keys()) : [];
    this.state = {
      from: options[0],
      to: options[0],
      rate: 1,
      options
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.swap = this.swap.bind(this);
  }

  handleInputChange(event) {
    let target = event.target;
    let value = target.value;
    let name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.updateRates(this.state.from, this.state.to, this.state.rate);
  }

  swap() {
    this.setState({
      from: this.state.to,
      to: this.state.from,
    });
  }

  render() {
    return(
      <div className="change-rates">
        <h2>Change rates</h2>
        <div className="rates-container">
          <div>
            <select
              value={this.state.from}
              name="from"
              onChange={this.handleInputChange}
              className="select-value" >
              {
                this.state.options.map(obj => {
                  return <option value={obj} key={uuid()}>{obj}</option>
                })
              }
            </select>
            <select
              value={this.state.to}
              name="to"
              onChange={this.handleInputChange}
              className="select-value" >
              {
                this.state.options.map(obj => {
                  return <option value={obj} key={uuid()}>{obj}</option>
                })
              }
            </select>
          </div>
          <FaArrowsV className="swap-arrows" onClick={this.swap}/>
        </div>
        <input
          name="rate"
          type="text"
          value={this.state.rate}
          onChange={this.handleInputChange}
          className="input-value" />
        <button type="submit" className="button" onClick={this.handleSubmit}><FaArrowRight/></button>
      </div>
    );
  }
}