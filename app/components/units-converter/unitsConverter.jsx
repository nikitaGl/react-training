import React from 'react';
import uuid from 'uuid';
import {Converter} from '../../converter/converter';
import './unitsConverter.scss';
import FaArrowRight from 'react-icons/lib/fa/arrow-right';
import FaArrowsH from 'react-icons/lib/fa/arrows-h';
import ConversionLoggerComponent from '../../components/conversion-logger-component/conversionLoggerComponent.jsx';

export default class UnitsConverter extends React.Component {
    constructor(props) {
        super(props);

        this.rootConverter = new Converter(props.converter, props.units);
        let options = props.units ? Array.from(props.units.keys()) : [];

        this.state = {
            input: '1',
            result: '',
            from: options[0],
            to: options[0],
            options
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleLogger = this.toggleLogger.bind(this);
        this.swap = this.swap.bind(this);
    }

    handleInputChange(event) {
        let target = event.target;
        let value = target.value;
        let name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        let converter = this.rootConverter.createConverter({from: this.state.from, to: this.state.to});
        let result =  converter ? converter.convert(this.state.input) : 'error';
        let date = new Date();

        this.props.addLogMessage(`[${date.getDate()}-${date.getMonth()}-${date.getFullYear()}][${date.getHours()}:${date.getMinutes()}]
            : from [${this.state.from}] => to [${this.state.to}]
            ${this.props.converter === 'money' ?
                ` | by rate: ${converter.units.get(this.state.from).get(this.state.to)}`
                :
                ``
            }
        `);

        this.setState({
            result
        });
    }

    swap() {
        this.setState({
            from: this.state.to,
            to: this.state.from
        });
    }

    toggleLogger(event) {
        this.props.toggleLogger(event.target.value);
    }

    render() {
        return (
            <div className="converter">
                <input type="checkbox" onChange={this.toggleLogger} checked={this.props.isLoggerOpen}/>Logger
                {
                    this.props.isLoggerOpen ?
                      <ConversionLoggerComponent messages={this.props.messages}
                                                 canRevert={this.props.canRevert}
                                                 revertLastChange={this.props.revertLastChange}/>
                      :
                      null
                }
                <div className="converter-container">
                    <form onSubmit={this.handleSubmit}>
                        <input
                          name="input"
                          type="text"
                          value={this.state.input}
                          onChange={this.handleInputChange}
                          className="input-value" />
                        <select
                          value={this.state.from}
                          name="from"
                          onChange={this.handleInputChange}
                          className="select-value" >
                            {
                                this.state.options.map(obj => {
                                    return <option value={obj} key={uuid()}>{obj}</option>
                                })
                            }
                        </select>
                        <FaArrowsH className="swap-arrows" onClick={this.swap}/>
                        <select
                          value={this.state.to}
                          name="to"
                          onChange={this.handleInputChange}
                          className="select-value" >
                            {
                                this.state.options.map(obj => {
                                    return <option value={obj} key={uuid()}>{obj}</option>
                                })
                            }
                        </select>
                        <button type="submit" className="button"><FaArrowRight/></button>
                    </form>
                    {
                        this.state.result ?
                          <div className="result">
                              {this.state.result}
                          </div>
                          : null
                    }
                </div>
            </div>

        );
    }
};