import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import './headerComponent.scss';
import logo from '../../assets/logo.svg';

export default class HeaderComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="header-component">
                <Link to="/"><img src={logo} className="logo" alt="logo" /></Link>
                <div className="header-container">
                    <h1>React converter</h1>
                    <div className="navbar">
                        <div className="bar">
                            <NavLink activeClassName="active" to={{pathname: '/money'}}>Money</NavLink>
                        </div>
                        <div className="bar">
                            <NavLink activeClassName="active" to={{pathname: '/time'}}>Time</NavLink>
                        </div>
                        <div className="bar">
                            <NavLink activeClassName="active" to={{pathname: '/length'}}>Length</NavLink>
                        </div>
                        <div className="bar">
                            <NavLink activeClassName="active" to={{pathname: '/mass'}}>Mass</NavLink>
                        </div>
                        <div className="bar">
                            <NavLink activeClassName="active" to={{pathname: '/log'}}>Log</NavLink>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}