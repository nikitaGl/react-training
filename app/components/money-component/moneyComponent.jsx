import React from 'react';
import './moneyComponent.scss';
import UnitsConverter from '../units-converter/unitsConverter.jsx';
import ModalComponent from '../modal-component/modalComponent.jsx';
import ChangeRatesComponent from '../change-rates-component/changeRatesComponent.jsx';

export default class MoneyComponent extends React.Component {
  constructor(props) {
    super(props);

    this.open = this.open.bind(this);
  }

  open() {
    this.refs.changeRatesModal.open();
  }

  render() {
    return (
      <div>
        <button className="button" onClick={this.open}>Change rates</button>
        <ModalComponent ref="changeRatesModal">
            <ChangeRatesComponent units={this.props.units} updateRates={(fromUnit, toUnit, rate) => this.props.updateRates(fromUnit, toUnit, rate)}/>
        </ModalComponent>
        <UnitsConverter converter='money'
                        units={this.props.units}
                        toggleLogger={this.props.toggleLogger}
                        isLoggerOpen={this.props.isLoggerOpen}
                        canRevert={this.props.canRevert}
                        revertLastChange={this.props.revertLastChange}
                        addLogMessage={this.props.addLogMessage}
                        messages={this.props.messages}/>
      </div>
    );
  }
}