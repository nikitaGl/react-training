import React from 'react';
import uuid from 'uuid';
import './conversionLoggerComponent.scss'

export default class ConversionLoggerComponent extends React.Component {
  constructor(props) {
    super(props);

    this.revertChange = this.revertChange.bind(this);
  }

  revertChange() {
    this.props.revertLastChange();
  }

  render() {
    return(
      <div className="conversion-logger">
          {
            this.props.messages.map((message, index) =>
              <div key={uuid()} style={{background: message.bgColor, color: message.color}}>{message.text}
                {
                    (this.props.canRevert) && (index === 0) ?
                      <span className="revert-changes" onClick={this.revertChange}>Revert change</span>
                      :
                      null
                }
              </div>
            )
          }
      </div>
    );
  }
}
