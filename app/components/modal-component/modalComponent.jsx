import React from 'react';
import './modalComponent.scss';
import FaTimesCircle from 'react-icons/lib/fa/times-circle';

export default class ModalComponent extends React.Component {
  constructor(props) {
    super(props);

    this.close = this.close.bind(this);
    this.open = this.open.bind(this);

    this.state = {
      isOpen: false
    };

  }

  open() {
    this.setState({
      isOpen: true
    });
  }

  close() {
    this.setState({
      isOpen: false
    });
  }


  render() {
    return(
      this.state.isOpen ?
        <div className="modal-container">
          <div className="modal">
            <FaTimesCircle className="times" onClick={this.close}/>
            {this.props.children}
          </div>
          <div className="backdrop" onClick={this.close}></div>
        </div>
        : null
    );
  }

}