import {combineReducers} from 'redux';
import converter from './converter';
import logger from './logger';

const rootReducer = combineReducers({
  converter,
  logger
});

export default rootReducer;