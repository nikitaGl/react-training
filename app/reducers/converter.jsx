import {MONEY_UNITS, MASS_UNITS, LENGTH_UNITS, TIME_UNITS} from '../converter/units/';
import * as types from '../shared/actionTypes';

const initialState = {
  units: {
    MONEY_UNITS,
    MASS_UNITS,
    LENGTH_UNITS,
    TIME_UNITS
  },
};

export default function converter(state = initialState, action) {
  switch(action.type) {
    case types.UPDATE_RATES:
      state.units.MONEY_UNITS.set(action.fromUnit, state.units.MONEY_UNITS.get(action.fromUnit).set(action.toUnit, action.rate));
      return state;
    default:
      return state;
  }
}