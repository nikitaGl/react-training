import * as types from '../shared/actionTypes';

const initialState = {
  isOpen: false,
  canRevert: false,
  messages: [
      {
          text: 'Logger...',
          bgColor: 'white',
          color: 'black'
      },
  ]
};

export default function logger (state = initialState, action) {
  switch(action.type) {
    case types.ADD_LOG_MESSAGE: {
      let newState = JSON.parse(JSON.stringify(state));
      let bgColor = getRandomColor();
      let color = invertColor(bgColor);

      newState.messages.unshift({
          text: action.message,
          bgColor,
          color
      });
      newState.canRevert = true;

      return newState;
    }
    case types.TOGGLE_LOGGER: {
      let newState = JSON.parse(JSON.stringify(state));
      newState.isOpen = !newState.isOpen;
      return newState;
    }
    case types.REVERT_LAST_CHANGE: {
      let newState = JSON.parse(JSON.stringify(state));
      newState.canRevert = false;
      newState.messages.shift();
      return newState;
    }
    default:
      return state;
  }
}

function invertColor(hex) {
  hex = hex.slice(1);

  let r = parseInt(hex.slice(0, 2), 16);
  let g = parseInt(hex.slice(2, 4), 16);
  let b= parseInt(hex.slice(4, 6), 16);

  return (r * 0.299 + g * 0.587 + b * 0.114) > 186
    ? '#000000'
    : '#FFFFFF';
}

function padZero(str, len) {
  len = len || 2;
  let zeros = new Array(len).join('0');
  return (zeros + str).slice(-len);
}

function getRandomColor() {
  return `#${padZero(Math.round(Math.random() * 0x1000000).toString(16), 6)}`;
}
