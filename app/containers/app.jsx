import React from 'react';
import {HashRouter as Router, Route, hashHistory} from 'react-router-dom';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../actions/';
import UnitsConverter from '../components/units-converter/unitsConverter.jsx';
import HeaderComponent from '../components/header-component/headerComponent.jsx';
import MoneyComponent from '../components/money-component/moneyComponent.jsx';
import ConversionLoggerComponent from '../components/conversion-logger-component/conversionLoggerComponent.jsx';
import '../shared/scss/index.scss';

class App extends React.Component {
  render() {
    return (
      <Router history={hashHistory}>
        <div className="router-container">
          <Route path='/' render={() => <HeaderComponent/>}/>
            <Route path='/money' render={() =>
              <MoneyComponent units={this.props.moneyUnits}
                              updateRates={this.props.actions.updateRates}
                              toggleLogger={this.props.actions.toggleLogger}
                              isLoggerOpen={this.props.isLoggerOpen}
                              canRevert={this.props.canRevert}
                              revertLastChange={this.props.actions.revertLastChange}
                              addLogMessage={this.props.actions.addLogMessage}
                              messages={this.props.messages}/>
            }/>
            <Route path="/time" render={() =>
              <UnitsConverter converter='time'
                              units={this.props.timeUnits}
                              toggleLogger={this.props.actions.toggleLogger}
                              isLoggerOpen={this.props.isLoggerOpen}
                              canRevert={this.props.canRevert}
                              revertLastChange={this.props.actions.revertLastChange}
                              addLogMessage={this.props.actions.addLogMessage}
                              messages={this.props.messages}/>
            }/>
            <Route path='/length' render={() =>
              <UnitsConverter converter='length'
                              units={this.props.lengthUnits}
                              toggleLogger={this.props.actions.toggleLogger}
                              isLoggerOpen={this.props.isLoggerOpen}
                              canRevert={this.props.canRevert}
                              revertLastChange={this.props.actions.revertLastChange}
                              addLogMessage={this.props.actions.addLogMessage}
                              messages={this.props.messages}/>
            }/>
            <Route path='/mass' render={() =>
              <UnitsConverter converter='mass'
                              units={this.props.massUnits}
                              toggleLogger={this.props.actions.toggleLogger}
                              isLoggerOpen={this.props.isLoggerOpen}
                              canRevert={this.props.canRevert}
                              revertLastChange={this.props.actions.revertLastChange}
                              addLogMessage={this.props.actions.addLogMessage}
                              messages={this.props.messages}/>
            }/>
          <Route path='/log' render={() =>
            <ConversionLoggerComponent messages={this.props.messages}
                                       revertLastChange={this.props.actions.revertLastChange}
                                       canRevert={this.props.canRevert}/>}/>
        </div>
      </Router>
    );
  }
}


function mapStateToProps(state) {
  return {
    data: state,
    isLoggerOpen: state.logger.isOpen,
    canRevert: state.logger.canRevert,
    messages: state.logger.messages,
    moneyUnits: state.converter.units.MONEY_UNITS,
    timeUnits: state.converter.units.TIME_UNITS,
    lengthUnits: state.converter.units.LENGTH_UNITS,
    massUnits: state.converter.units.MASS_UNITS
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);